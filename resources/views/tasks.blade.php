<!DOCTYPE html>
<html lang="en">
<head>
    <title>Tasks</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<h1 align="center"> To Do </h1>
</head>
<body>

<div class="container">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Task</th>
            <th>Status</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($tasks as $task)
            <tr>
            <td>{{ $task->body }}</td>
            <td>
                <form action="{{ url('tasks/'.$task->id) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <button type="submit" id="update-{{ $task->id }}" class="btn btn-primary">
                @if ($task->status === 1)
                  Complete
                @else
                Incomplete
                @endif
                    </button>
                </form>
            </td>
                <td>
                    <form action="{{ url('tasks/'.$task->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="submit" id="delete-task-{{ $task->id }}" class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<form action="{{ url('/tasks/store') }}" method="POST" class="form-horizontal">
{{ csrf_field() }}

    <div class="form-group">
        <label for="task-name" class="col-sm-3 control-label"> Create New Task:</label>

        <div class="col-sm-6">
            <input type="text" name="body" id="task-name" class="form-control">
        </div>
    </div>

    <!-- Add Task Button -->

    <div align="center" class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus"></i> Add Task
            </button>
        </div>
    </div>
</form>

<!-- Home Button -->
<p align="center">
<button type="button" onclick="window.location='{{url("/")}}'">Home</button>
</p>
</body>
</html>