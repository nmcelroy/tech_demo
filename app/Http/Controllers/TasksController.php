<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: nicolemcelroy
 * Date: 8/31/17
 * Time: 4:12 PM
 */
use App\Http\Controllers\Controller;
use DB;
use App\Task;
use Illuminate\Http\Request;

class TasksController extends Controller {

    public function index()
    {
        $tasks = DB::table('tasks')->get();
        $this->viewData['tasks'] = $tasks;
        return view('tasks', $this->viewData);
    }


    public function store(Request $request){
      $this->validate($request, [
            'body' => 'required|max:255'
        ]);
        $task = new Task;
        $task->body = $request->body;
        $task->status = 0;
        $task->save();
        return redirect('/tasks');
    }

    public function destroy( Task $task){
       $task->delete();
       return redirect('/tasks');
    }

    public function update(Task $task){
        if ($task->status === 1) {
            $task->status = 0;
        }
        else{
            $task->status = 1;
        }
        $task->save();
        return redirect('/tasks');
    }
}
