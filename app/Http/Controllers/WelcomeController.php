<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: nicolemcelroy
 * Date: 8/31/17
 * Time: 4:10 PM
 */
use App\Http\Controllers\Controller;

class WelcomeController extends Controller {
    //mapped to http://laravel.app

    public function index()
    {
        $name = "Nicole";
        //load welcome view
        return view('welcome', ['name'=>$name]);
    }
}