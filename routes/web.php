<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('/tasks','TasksController@index');

Route::delete('/tasks/{task}', 'TaskController@destroy');

Route::post('/tasks/store', 'TasksController@store');

Route::post('/tasks/{task}', 'TasksController@update');

Route::resource('tasks', 'TasksController',['only' => ['store', 'update',
    'destroy', 'index'
]]);

// comment for github